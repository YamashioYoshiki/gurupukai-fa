package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserHobbyDao;

/** UserHobbyDaoTest
 * @author AZUMA Ginji
 */
@WebServlet(name="UserHobbyDaoTest", urlPatterns={"/UserHobbyDaoTest.java"})
public class UserHobbyDaoTest extends HttpServlet {
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// searchHobbyNamesByUserId
		printTestTitle("searchHobbyNamesByUserId");
		searchHobbyNamesByUserIdTest(1);
		searchHobbyNamesByUserIdTest(2);
		searchHobbyNamesByUserIdTest(-1);

		// updateHobbyNameOfUser
		printTestTitle("updateHobbyNameOfUser");
		List<String> hobbyNames = new ArrayList<>(Arrays.asList("酒", "スポーツ"));
		updateHobbyNameOfUserTest(1, hobbyNames);
//		updateHobbyNameOfUserTest(-1, hobbyNames); // ここは外部キー制約エラー

		// deleteHobbyOfUser & insert
		printTestTitle("deleteHobbyOfUser");
		deleteHobbyOfUserTest(1);
		deleteHobbyOfUserTest(-1);
		printTestTitle("insert");
		insertTest(1, Arrays.asList("酒", "スポーツ", "音楽"));
//		insertTest(-1, Arrays.asList("音楽")); // ここは外部キー制約エラー
	}

	public static void printTestTitle(String title) {
		System.out.println("\n\n////////////////////////////// " + title + "  //////////////////////////////");
	}

	// searchHobbyNamesByUserId のテスト
	public static void searchHobbyNamesByUserIdTest(int userId) {
		UserHobbyDao dao = new UserHobbyDao();
		List<String> hobbyNames = dao.searchHobbyNamesByUserId(userId);
		System.out.print("ID " + userId + " のユーザの趣味: ");
		for (String hobbyName : hobbyNames) {
			System.out.print(hobbyName + ", ");
		}
		System.out.println();
	}

	// updateHobbyNameOfUser のテスト
	public static void updateHobbyNameOfUserTest(int userId, List<String> hobbyNames) {
		UserHobbyDao dao = new UserHobbyDao();
		dao.updateHobbyNameOfUser(userId, hobbyNames);
		hobbyNames = dao.searchHobbyNamesByUserId(userId);
		searchHobbyNamesByUserIdTest(userId);
	}

	// insert のテスト
	public static void insertTest(int userId, List<String> hobbyNames) {
		UserHobbyDao dao = new UserHobbyDao();
		dao.insert(userId, hobbyNames);
		searchHobbyNamesByUserIdTest(userId);
	}

	// deleteHobbyOfUser のテスト
	public static void deleteHobbyOfUserTest(int userId) {
		UserHobbyDao dao = new UserHobbyDao();
		dao.deleteHobbyOfUser(userId);
		searchHobbyNamesByUserIdTest(userId);
	}
}
