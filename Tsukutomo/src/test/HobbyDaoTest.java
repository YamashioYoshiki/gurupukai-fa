package test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HobbyDao;

/** HobbyDaoTest
 * @author AZUMA Ginji
 */
@WebServlet(name = "HobbyDaoTest", urlPatterns = { "/HobbyDaoTest.java" })
public class HobbyDaoTest extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// selectAll
		printTestTitle("selectAll");
		HobbyDao dao = new HobbyDao();
		HashMap<Integer, String> hobbys = dao.selectAll();
		for (Iterator<Map.Entry<Integer, String>> iterator = hobbys.entrySet().iterator(); iterator.hasNext();) {
			Map.Entry<Integer, String> entry = iterator.next();
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

		// searchHobbyIdByHobbyName
		printTestTitle("searchHobbyIdByHobbyName");
		searchHobbyIdByHobbyNameTest("酒");
		searchHobbyIdByHobbyNameTest("スポーツ");
		searchHobbyIdByHobbyNameTest("ほげ");
	}

	public static void printTestTitle(String title) {
		System.out.println("\n\n////////////////////////////// " + title + "  //////////////////////////////");
	}

	// searchHobbyIdByHobbyName のテスト
	public static void searchHobbyIdByHobbyNameTest (String hobbyName) {
		HobbyDao dao = new HobbyDao();
		int hobbyId = dao.searchHobbyIdByHobbyName(hobbyName);
		System.out.println("趣味名: " + hobbyName + "    ID: " + hobbyId + ": ");
	}
}
