package dto;

import java.util.List;

public class UserDto {
	private int userId = -1;//ユーザーID int auto_increment
	private String userName;//ユーザーの名前 varchar(50) unique
	private String mailAddress;//メールアドレスvachar(100) unique
	private String password;//パスワード password char(64)
	private int gender = -1;//性別 gender int(1)
	private List<String> hobbyName;//趣味の名前 int auto_increment

	public void view() {//デバッグ用　全フィールド表示
		System.out.println("print UserDto");
		System.out.println("user id: " + this.userId);
		System.out.println("user name: " + this.userName);
		System.out.println("mail address: " + this.mailAddress);
		System.out.println("password: " + this.password);
		System.out.println("gender: " + this.gender);
		System.out.print("user hobbys: ");
		for (String s : hobbyName) {
			System.out.print(s + ", ");
		}
		System.out.println();

	}

	public UserDto() {
		;
	}

	public UserDto(String userName, String mailAddress, String password, int gender,
			List<String> hobbyName) {
		this.userName = userName;
		this.mailAddress = mailAddress;
		this.password = password;
		this.gender = gender;
		this.hobbyName = hobbyName;
	}


	public int getUserId() {//ここからGettre一覧
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public String getPassword() {
		return password;
	}

	public int getGender() {
		return gender;
	}

	public List<String> getHobbyName() {
		return hobbyName;
	}

	public void setUserId(int userId) {//ここからSetter一覧
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public void setHobbyName(List<String> hobbyName) {
		this.hobbyName = hobbyName;
	}

	public boolean isEmpty() {
		boolean ret = false;
		if (userId == -1 && userName == null && mailAddress == null
				&& password == null && gender == -1 && hobbyName.isEmpty()) {
			ret = true;
		}
		return ret;
	}
}
