package dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class EventDto {
	private int eventId = -1;//	id int auto_increment
	private String title;//タイトル title varchar(100)
	private String content;//本文 content varchar(300)
	private int currentEntry = -1;//現在の参加人数 TBとの連携なし
	private int maxEntry = -1;//人数制限 max_entry int
	private Timestamp eventDate;//イベント期日
	private List<String> hobbys = new ArrayList<String>();//趣味タグ一覧
	private List<String> entryUsersName = new ArrayList<String>();//イベント参加人数一覧
	private UserDto createUser;//イベントの作成者

	public void view() {//デバッグ用　全フィールド表示
		System.out.println("------------------");
		System.out.println("print EventDto");
		System.out.println("event Id: " + this.eventId);
		System.out.println("event title: " + this.title);
		System.out.println("-----");
		this.createUser.view();
		System.out.println("-----");
		System.out.println("content: " + this.content);
		System.out.println("current entry: " + this.currentEntry);
		System.out.println("max entry: " + this.maxEntry);
		System.out.println("event date: " + this.eventDate);
		System.out.print("event hobbys: ");
		for (String s : hobbys) {
			System.out.print(s + ", ");
		}
		System.out.println();
		System.out.print("entry user names: ");
		for (String s : entryUsersName) {
			System.out.print(s + ", ");
		}
		System.out.println();
	}

	public int getEventId() {//ここからgetter
		return eventId;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public int getCurrentEntry() {
		return currentEntry;
	}

	public int getMaxEntry() {
		return maxEntry;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public List<String> getHobbys() {
		return hobbys;
	}

	public List<String> getEntryUsersName() {
		return entryUsersName;
	}

	public UserDto getCreateUser() {
		return createUser;
	}

	public void setEventId(int eventId) {//ここからsetter
		this.eventId = eventId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCurrentEntry(int currentEntry) {
		this.currentEntry = currentEntry;
	}

	public void setMaxEntry(int maxEntry) {
		this.maxEntry = maxEntry;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public void setHobbys(List<String> hobbys) {
		this.hobbys = hobbys;
	}

	public void setEntryUsersName(List<String> entryUsersName) {
		this.entryUsersName = entryUsersName;
	}

	public void setCreateUser(UserDto createUser) {
		this.createUser = createUser;
	}

	public boolean isEmpty() {
		boolean ret = false;
		if (eventId == -1 && title == null && content == null
				&& currentEntry == -1 && maxEntry == -1 && eventDate == null
				&& hobbys.isEmpty() && entryUsersName.isEmpty() && createUser == null) {
			ret = true;
		}
		return ret;
	}
}
