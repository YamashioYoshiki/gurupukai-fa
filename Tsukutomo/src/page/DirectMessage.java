package page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.UserDto;

@WebServlet(name="direct", urlPatterns={"/direct/*"})
public class DirectMessage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		UserDto user = (UserDto)session.getAttribute("user");

		if (user == null) {
			response.sendRedirect("/Tsukutomo/login.jsp");
		} else {
			request.setAttribute("from", user.getUserName());
			request.setAttribute("to", request.getPathInfo() == null ? user.getUserName() : request.getPathInfo().replace("/", ""));
			request.getRequestDispatcher("/WEB-INF/jsp/directMessage.jsp").forward(request, response);
		}
	}
}
