package page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HobbyDao;
import dao.UserDao;
import dto.UserDto;

@WebServlet(name="userRegistration", urlPatterns={"/userRegistration"})
public class UserRegistrationAction extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		HobbyDao hobbyDao = new HobbyDao();
		ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();

		request.setAttribute("seleted", new ArrayList<String>());
		request.setAttribute("hobbyList", hobbyNameList);
		request.getRequestDispatcher("userRegistration.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HobbyDao hobbyDao = new HobbyDao();
		ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();

		request.setAttribute("hobbyList", hobbyNameList);

		String name = request.getParameter("name");
		Integer gender = Integer.parseInt(request.getParameter("gender"));
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String[] hobbys = request.getParameterValues("hobby");

		String error = "";
		if (!password.equals(confirmPassword)) error = "パスワードが一致しません";
		if (hobbys == null) error = "趣味が未入力です";

		UserDto user = null;
		if (error.equals("")) {
			user = new UserDto(name, mail, password, gender, Arrays.asList(hobbys));
			if (new UserDao().insert(user) == -1) {
				error = "名前またはメールアドレスが重複しています";
			}
		}

		if (error.equals("") && user != null) {
			user.setUserId(new UserDao().getUserId(user.getUserName()));
			user.setPassword("");
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			response.sendRedirect("/Tsukutomo/index");
		} else {
			request.setAttribute("name", name);
			request.setAttribute("mail", mail);
			request.setAttribute("password", password);
			request.setAttribute("confirmPassword", confirmPassword);
			request.setAttribute("gender", gender);
			if (hobbys == null) hobbys = new String[0];
			request.setAttribute("seleted", Arrays.asList(hobbys));

			System.out.println(error);

			request.setAttribute("error", error);
			request.getRequestDispatcher("userRegistration.jsp").forward(request, response);
		}
	}
}
