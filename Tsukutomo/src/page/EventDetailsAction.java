package page;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dto.EventDto;
import dto.UserDto;

@WebServlet(name="eventDetails", urlPatterns={"/eventDetails/*"})
public class EventDetailsAction extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		UserDto user = (UserDto)session.getAttribute("user");

		String path = req.getPathInfo();
		Integer eventId = -1;
		try {
			eventId = Integer.parseInt(path.replace("/", ""));
		} catch (Exception e) {
			res.sendRedirect("/Tsukutomo/index");
		}

		if (user == null) {
			res.sendRedirect("/Tsukutomo/login");
		} else {
			if (eventId != -1) {
				EventDto event = new EventDao().selectById(eventId);
				if (event == null || event.isEmpty()) {
					res.sendRedirect("/Tsukutomo/index");
				} else {
					req.setAttribute("eventId", eventId);
					req.setAttribute("hobbys", event.getHobbys());
					req.setAttribute("title", event.getTitle());
					req.setAttribute("ownerName", event.getCreateUser().getUserName());
					req.setAttribute("ownerId", event.getCreateUser().getUserId());
					Timestamp time = (Timestamp) event.getEventDate();
					SimpleDateFormat sdf = new SimpleDateFormat("YYYY年MM月dd日 HH:mm (E)");
					req.setAttribute("eventDate", sdf.format(time));
					req.setAttribute("entryUser", event.getEntryUsersName().size());
					req.setAttribute("entryUserNames", event.getEntryUsersName());
					req.setAttribute("maxEntry", event.getMaxEntry());
					req.setAttribute("text", event.getContent());

					if (event.getEntryUsersName().contains(user.getUserName())) {
						req.setAttribute("isJoin", true);
					} else {
						req.setAttribute("isJoin", false);
					}

					req.getRequestDispatcher("/WEB-INF/jsp/eventDetails.jsp").forward(req, res);
				}
			}
		}
	}
}
