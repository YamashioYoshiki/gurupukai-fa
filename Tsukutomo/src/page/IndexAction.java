package page;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dao.HobbyDao;
import dto.EventDto;
import dto.UserDto;

@WebServlet(name="index", urlPatterns={"/index"})
public class IndexAction extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int gender = -1;
		List<String> hobby = null;

		HttpSession session = request.getSession();
		UserDto user =(UserDto) session.getAttribute("user");
		String[] selectedHobby = request.getParameterValues("hobby");

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		if (user != null) gender = user.getGender();
		if (selectedHobby != null) hobby = Arrays.asList(request.getParameterValues("hobby"));

		ArrayList<EventDto> eventList = getEvents(gender, hobby, stringToTimestamp(startDate), stringToTimestamp(endDate));
		HobbyDao hobbyDao = new HobbyDao();
		ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();

		request.setAttribute("eventList", eventList);
		request.setAttribute("hobbyList", hobbyNameList);

	    String view = "/index.jsp";
	    RequestDispatcher dispatcher = request.getRequestDispatcher(view);

	    dispatcher.forward(request, response);
	}

	protected ArrayList<EventDto> getEvents(int gender, List<String> userHobbys, Timestamp startDate, Timestamp endDate){
		EventDao eventDao = new EventDao();
		ArrayList<EventDto> resultList = new ArrayList<EventDto>();
		ArrayList<EventDto> eventList = new ArrayList<EventDto>();

		//ユーザが登録している趣味と合致するイベントを取得
		if (userHobbys == null) {
			eventList.addAll(eventDao.selectAll());
		} else {
			for (String hobby : userHobbys) {
				eventList.addAll(eventDao.selectByHobby(hobby));
			}
		}

		if (startDate != null && endDate != null) {
			eventList = (ArrayList<EventDto>)eventList.stream()
					.filter(event -> event.getEventDate().after(startDate) &&
									  event.getEventDate().before(new Timestamp(endDate.getTime() + 24 * 3600 * 1000))).collect(Collectors.toList());
		} else if (startDate != null) {
			eventList = (ArrayList<EventDto>)eventList.stream()
					.filter(event -> event.getEventDate().after(startDate)).collect(Collectors.toList());
		} else if (endDate != null){
			eventList = (ArrayList<EventDto>)eventList.stream()
					.filter(event -> event.getEventDate().before(new Timestamp(endDate.getTime() + 24 * 3600 * 1000))).collect(Collectors.toList());
		}

		if (!(eventList.isEmpty())) {
			if (gender != -1) {
				//ユーザと性別が合致するイベントだけ取得
				for (EventDto event : eventList) {
					if (gender == event.getCreateUser().getGender()) {
						ArrayList<String> hobbys = eventDao.hobbySelectById(event.getEventId());
						event.setHobbys(hobbys);
						resultList.add(event);
					}
				}
			} else {
				for(EventDto event : eventList) {
					ArrayList<String> hobbys = eventDao.hobbySelectById(event.getEventId());
					event.setHobbys(hobbys);
					resultList.add(event);
				}
			}
		} else {
			System.out.println("イベントが見つかりません。");
		}

		return (ArrayList<EventDto>) resultList.stream().filter(distinctByKey(EventDto::getEventId)).collect(Collectors.toList());
	}

	private Timestamp stringToTimestamp(String d) {
		Timestamp result = null;
		try {
			result = new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(d).getTime());
		} catch (Exception e) {
			result = null;
		}

		return result;
	}

	private <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
	    Set<Object> seen = ConcurrentHashMap.newKeySet();
	    return t -> seen.add(keyExtractor.apply(t));
	}
}
