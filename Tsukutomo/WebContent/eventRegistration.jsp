<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>
<body class="bg-light">
	<jsp:include page="menu.jsp" flush="true" />
	<%
		ArrayList<String> hobbyList = (ArrayList<String>) request.getAttribute("hobbyList");
		String error = (String) request.getAttribute("error");
	%>

	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<div class="col-lg-8 container mb-3">
				<div class="row">
					<div class="col-lg  text-center">
						<h1>イベント登録フォーム</h1>
					</div>
				</div>
				<div class="row">
				<%
					if (error != null){%>
						<div class="col-lg">
							<p style="color: red; font-size: larger;"><%=error %></p>
						</div>
				<%}%>
				</div>
				<div class="row">
					<form method="post" action="eventRegistration">
						<div class="form-group">
							<label for="title">イベント名 </label>
							<input type="text" id="tlte" name="eventName" required
							class="form-control" size="100" placeholder="例：鍋パーティ">
						</div>
						<div class="from-group row">
							<div class="col-lg">
								募集の締め切り日時
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="day">年月日</label>
								<input type="date" class="form-control" name="startDate" id="date" required
									min="1970-01-01"max="2038-01-09" />
							</div>
							<div class="col-lg">
								<label for="day">時</label>
								<div class="input-group">
									<input type="text" class="form-control" name="hour" id="hour" required
										pattern="^([01][0-9]|2[0-3]|[0-9])$" placeholder="18">
									<div class="input-group-prepend">
										<span class="input-group-text">時</span>
									</div>
								</div>
							</div>
							<div class="col-lg">
								<label for="day"> 分</label>
								<div class="input-group">
									<input type="text" class="form-control" name="min" id="min" required
										pattern="^([0-5][0-9]|[0-9])$" placeholder="30">
									<div class="input-group-prepend">
										<span class="input-group-text">分</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="day"> 制限人数(人)</label>
								<div class="input-group">
									<input type="number" class="form-control" name="peopleRestriction" required
										id="max_entry" placeholder="例：30"min="1" max="9999">
									<div class="input-group-prepend">
										<span class="input-group-text">人</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="hobby_id">趣味タグ</label>
							</div>
						</div>
						<div class="form-group row">
							<%
								for (String hobby : hobbyList) {
									System.out.println(hobby);
							%>
								<div class="col-lg-3">
											<input type="checkbox" name="hobby" value=<%=hobby %>>
									<label><%=hobby %></label>
								</div>
							<%} %>
						</div>
						<div class="form-group">
							<label for="content">本文</label>
							<textarea id="content" name="text" class="form-control"rows="6" size="300" required></textarea>
						</div>
						<hr class="mb-4">
						<button class="btn btn-primary btn-lg btn-block" type="submit">イベント内容 登録確定</button>
					</form>
				</div>
			</div>
			<div class="col-lg"></div>
		</div>
	</div>
</body>
</html>