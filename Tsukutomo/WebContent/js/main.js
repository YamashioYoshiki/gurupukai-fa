'use strict';
let from = document.getElementById("from").value;
let to = document.getElementById("to").value;

function signIn() {
  const email = "park@test.com";
  const password = "parkpark";
  firebase.auth().signInWithEmailAndPassword(email, password)
      .catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    if (errorCode === 'auth/wrong-password') {
      alert('Wrong password.');
    } else {
      alert(errorMessage);
    }
    console.log(error);
  });
}

signIn();

function signOut() {
  firebase.auth().signOut();
  from = "";
}

function initFirebaseAuth() {
  firebase.auth().onAuthStateChanged(authStateObserver);
}

function getUserName() {
  return firebase.auth().currentUser.displayName;
}

function isUserSignedIn() {
  return !!firebase.auth().currentUser;
}

function saveMessage(messageText) {
  return firebase.firestore().collection('messages').add({
    from,
    to,
    text: messageText,
    timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }).catch(function(error) {
    console.error('Error writing new message to Firebase Database', error);
  });
}

function toggleToFrom() {
	from = document.getElementById("from").value;
	to = document.getElementById("to").value;
}

let userList = [];
const USER_LIST_TEMPLATE = (name) =>
    `<button type="button" class="btn btn-primary" onclick="toggleUser('${name}');">${name}</button>`;

function loadUserList() {

  const query = firebase.firestore().collection('messages')
                  .orderBy('timestamp', 'desc')
                  .limit(12);


  query.onSnapshot(function(snapshot) {
    snapshot.forEach(function(doc) {
    	let targetTo = doc.data().to;
    	let targetFrom = doc.data().from;

    	if (!userList.includes(targetTo) && targetFrom === from) {
    		userList.push(targetTo);
    	}
    	if (!userList.includes(targetFrom) && targetTo === from) {
    		userList.push(targetFrom);
    	}

    });

    const container = document.getElementById('user-list');
    container.innerHTML = '';
    userList.sort().forEach(function(target) {
  	  container.innerHTML += USER_LIST_TEMPLATE(target);
    })
  });


}

function loadMessages() {
	loadUserList();

  const query = firebase.firestore().collection('messages')
                  .orderBy('timestamp', 'desc')
                  .limit(12);

  query.onSnapshot(function(snapshot) {
    snapshot.forEach(function(doc) {
      if (doc.type === 'removed') {
        deleteMessage(doc.id);
      } else {
        var message = doc.data();
        if ((message.from == from && message.to == to) || (message.from == to && message.to == from)) {
          displayMessage(doc.id, message.timestamp, message.from, message.text);
        }
      }
    });
  });
}

function onMediaFileSelected(event) {
  event.preventDefault();
  var file = event.target.files[0];
  imageFormElement.reset();

  if (!file.type.match('image.*')) {
    var data = {
      message: 'You can only share images',
      timeout: 2000
    };
    signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
    return;
  }

  if (checkSignedInWithMessage()) {
    saveImageMessage(file);
  }
}

function onMessageFormSubmit(e) {
  e.preventDefault();
  if (messageInputElement.value && checkSignedInWithMessage()) {
    saveMessage(messageInputElement.value).then(function() {
      // Clear message text field and re-enable the SEND button.
      resetMaterialTextfield(messageInputElement);
      toggleButton();
    });
  }
}

// Triggers when the auth state change for instance when the user signs-in or signs-out.
function authStateObserver(user) {}

// Returns true if user is signed-in. Otherwise false and displays a message.
function checkSignedInWithMessage() {
  // Return true if the user is signed in Firebase
  if (isUserSignedIn()) {
    return true;
  }

  // Display a message to the user using a Toast.
  var data = {
    message: 'You must sign-in first',
    timeout: 2000
  };
  signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
  return false;
}

function resetMaterialTextfield(element) {
  element.value = '';
}

// Template for messages.
var MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="message"></div>' +
      '<div class="name"></div>' +
    '</div>';

// Adds a size to Google Profile pics URLs.
function addSizeToGoogleProfilePic(url) {
  if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
    return url + '?sz=150';
  }
  return url;
}

// A loading image URL.
var LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';

// Delete a Message from the UI.
function deleteMessage(id) {
  var div = document.getElementById(id);
  // If an element for that message exists we delete it.
  if (div) {
    div.parentNode.removeChild(div);
  }
}

function deleteMessageAll() {
	const messages = firebase.firestore().collection('messages');
	var query = messages
	    .orderBy('timestamp', 'desc')
	    .limit(12);

query.onSnapshot(function(snapshot) {
    snapshot.forEach(function(doc) {
    	deleteMessage(doc.id);
    });
  });
}

// Displays a Message in the UI.
function displayMessage(id, timestamp, name, text) {
  var div = document.getElementById(id);
  // If an element for that message does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', id);
    div.setAttribute('timestamp', timestamp);
    for (var i = 0; i < messageListElement.children.length; i++) {
      var child = messageListElement.children[i];
      var time = child.getAttribute('timestamp');
      if (time && time > timestamp) {
        break;
      }
    }
    messageListElement.insertBefore(div, child);
  }

  div.querySelector('.name').textContent = name;
  var messageElement = div.querySelector('.message');
  if (text) {
    messageElement.textContent = text;
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  }
  setTimeout(function() {div.classList.add('visible')}, 1);
  messageListElement.scrollTop = messageListElement.scrollHeight;
  messageInputElement.focus();
}

function toggleButton() {
  if (messageInputElement.value) {
    submitButtonElement.removeAttribute('disabled');
  } else {
    submitButtonElement.setAttribute('disabled', 'true');
  }
}

var messageListElement = document.getElementById('messages');
var messageFormElement = document.getElementById('message-form');
var messageInputElement = document.getElementById('message');
var submitButtonElement = document.getElementById('submit');
var userNameElement = document.getElementById('user-name');

messageFormElement.addEventListener('submit', onMessageFormSubmit);

messageInputElement.addEventListener('keyup', toggleButton);
messageInputElement.addEventListener('change', toggleButton);

initFirebaseAuth();

loadMessages();
