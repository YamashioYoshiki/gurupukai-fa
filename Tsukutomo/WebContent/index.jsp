<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="dto.EventDto"%>
<%@ page import="dto.UserDto"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/common.css">
</head>
<body class="bg-light">
	<jsp:include page="menu.jsp" flush="true" />

	<%
		ArrayList<EventDto> eventList =  (ArrayList<EventDto>)request.getAttribute("eventList");
		ArrayList<String> hobbyList = (ArrayList<String>)request.getAttribute("hobbyList");
	%>

	<div class="mx-auto">
		<div class="container">
			<div class="row">
				<div class="col"></div>
				<div class="form-container col-lg-6 bg-white">
					<form action="index" method="post">
						<div class="form-group">
							<label id="label" class="col-form-label">日程</label>
							<div class="row">
								<div class="col-lg">
									<input type="date" class="form-control" name="startDate" />
								</div>
								<div class="col-lg text-center"><h2>～</h2></div>
								<div class="col-lg">
									<input type="date" class="form-control" name="endDate"/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label id="label" class="col-form-label">趣味</label>
							<div class="row">
								<% for (String hobby : hobbyList) { %>
									<div class="col-lg-3">
										<input type="checkbox" name="hobby" value="<%=hobby%>"><%=hobby%>
									</div>
								<% } %>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">検索</button>
						</div>
					</form>
				</div>
				<div class="col"></div>
			</div>

			<div class="row">
			    <div class="col"></div>
				<div class="event-container col-lg-8">
					<%for(EventDto event : eventList){ %>
						<div class="card m-3">
							<form action="eventDetails" method="post">
								<input type="hidden" name="eventId" value=<%=event.getEventId() %>/>
								<table class="table">
									<tr>
										<td colspan="2" style="border-top: none;">
											<% for (String hobby : event.getHobbys()) { %>
												<button type="button" class="btn btn-info" disabled><%=hobby%></button>
											<%} %>
										</td>
									</tr>
									<tr>
										<td><h4><%=event.getTitle() %></h4></td>
										<td align="right"><%=event.getCreateUser().getUserName() %></td>
									</tr>
									<tr>
										<td colspan="2"><%=event.getContent() %></td>
									</tr>
									<tr>
										<td colspan="2" align="right">
											<span id="label"><%=event.getCurrentEntry() %> / <%=event.getMaxEntry() %> 人</span>
                                        	<a href="/Tsukutomo/eventDetails/<%= event.getEventId() %>" class="btn btn-primary">詳細</a>										</td>
									</tr>
								</table>
							</form>
						</div>
					<% } %>
				</div>
				<div class="col"></div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>